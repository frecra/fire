require 'digest'
require 'net/http'
require 'json'


CLIENT_ID=ENV['CLIENT_ID']
CLIENT_KEY=ENV['CLIENT_KEY']
REFRESH_TOKEN=ENV['REFRESH_TOKEN']

base_url='https://api.paywithfire.com'

t = Time.now.strftime '%s'

SECRET=Digest::SHA256.hexdigest "#{t}#{CLIENT_KEY}"

request_params={:clientId => CLIENT_ID, :refreshToken => REFRESH_TOKEN, :nonce => t, :grantType => "AccessToken", :clientSecret => SECRET}


uri = URI base_url
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true if uri.scheme == 'https'

response = http.post('/business/v1/apps/accesstokens', request_params.to_json, initheader = {'Content-Type' => 'application/json'})

if response.code != '201'
  puts "Error: Response #{response.code} #{response.message}: #{response.body}"
  exit 1
end

response_hash = JSON.parse(response.body)
access_token = response_hash['accessToken']
accounts = http.get('/business/v1/accounts', initheader = {'Authorization' => "Bearer #{access_token}"})
if accounts.code != '200'
  puts "Error: Response #{accounts.code} #{accounts.message}: #{accounts.body}"
  exit 1
end

account_id=(JSON.parse(accounts.body))['accounts'][0]['ican']
txns = http.get("/business/v1/accounts/#{account_id}/payments", initheader = {'Authorization' => "Bearer #{access_token}"})


p txns.body

